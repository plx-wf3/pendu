FROM python:3.8

EXPOSE 5000

LABEL maintainer=XacMu

RUN pip3 install flask

# Add demo app
RUN mkdir /app
COPY ./app /app
WORKDIR /app

CMD [ "python3", "hello.py" ]