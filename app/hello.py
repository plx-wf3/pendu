from flask import Flask, request, render_template
import os
import random

app = Flask(__name__)

FILEPATH = "pendu.txt"

LISTE_MOT = [
    "armoire",
    "boucle",
    "buisson",
    "bureau",
    "chaise",
    "carton",
    "couteau",
    "fichier",
    "garage",
    "glace",
    "journal",
    "kiwi",
    "lampe",
    "liste",
    "montagne",
    "remise",
    "sandale",
    "taxi",
    "vampire",
    "volant",
]


@app.route('/', methods=["GET", "POST"])
def hello():
    if request.method == "POST":
        print(request.form.to_dict())
        return "Hello!"
    else:
        return render_template('template.html', titre="Bienvenue !")

@app.route('/new', methods=['GET', 'POST'])
def new_game():
    #if is_game_running():
    #    return "jeux en cours"

    #mot = LISTE_MOT[random.randrange(0, len(LISTE_MOT))]

    return request.data

    #return mot
   # return dict(request.headers)


@app.route('/texte', methods=['GET', 'POST'])
def test_letter():
    return request.form.to_dict()
    #return 'Texte'

@app.route('/proposition', methods=['GET', 'POST'])
def test_proposition():
    return 'Prop'

def is_game_running():
    return os.path.exists(FILEPATH)
    



if __name__ == '__main__':
    app.run(host="0.0.0.0")
